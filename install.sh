# bashrc aliases
cp .bash_aliases ~/.bash_aliases

# config git
git config --global user.name "Włodzimierz Ciesielski"
git config --global user.email vovagorodok2@gmail.com
git config --global alias.co checkout
git config --global alias.br branch
git config --global alias.ci commit
git config --global alias.st status
git config --global alias.lg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'"

# activate ssh pub key
[[ -e ~/.ssh/id_rsa.pub ]] && cp ~/.ssh/id_rsa.pub ~/.ssh/authorized_keys

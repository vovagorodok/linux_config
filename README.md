## Common
```bash
./install.sh
```

## Microsoft fonts
Link: https://www.zdnet.com/article/how-to-install-microsoft-fonts-on-linux-for-better-collaboration/

## Adwaita theme
```
sudo dnf install adw-gtk3-theme yaru-theme
```
Link: https://github.com/lassekongo83/adw-gtk3

## More Gnome wallpapers
Link: https://github.com/simple-sunrise/Light-and-Dark-Wallpapers-for-Gnome

## Gnome extensions
- AppIndicator and KStatusNotifierItem Support
- Luminus Desktop
- TopHat
- Blur my Shell
- Dash to Dock
- Desktop Icons NG

## Install snap on Fedora
```
sudo dnf install snapd
sudo ln -s /var/lib/snapd/snap /snap
sudo snap install snap-store
```
Link: https://snapcraft.io/docs/installing-snap-on-fedora